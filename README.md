# 4:12 Bootstrap
4:12 Productions Bootstrap Theme Based on Flatly Bootswatch

## Install

To install with bower, run the following:

```bash
npm install https://gitlab.com/nprail/412-bootstrap.git#v$VERSION_NUMBER
// e.g.
npm install https://gitlab.com/nprail/412-bootstrap.git#v1.1.0
```